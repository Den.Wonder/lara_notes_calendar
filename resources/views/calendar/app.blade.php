<div>
    <p>today is {{$str_date}}</p>

    <div class="calendar-caption">
        <h3 class="align-items-center pb-3">{{$input_date->format('F Y')}}</h3>
    </div>
    <div class="calendar-header">
        <ul class="week_headings">
            @foreach($headings as $heading)
                <li class="calendar_head_elements"><div>{{$heading}}</div></li>
            @endforeach
        </ul>
    </div>
    <div class="calendar-section">
        <form method="get">
            @csrf
        <ul class="week">
        @if($dt->format('N')!=1)
            @for ($i=1; $i<$dt->format('N'); $i++)
                <div><li class="day"></li></div>
            @endfor
        @endif
        @for($i=0;$i<$days_count;$i++)
            @if($dt->format('N')==1)
        </ul><ul class="week">
            @endif
            <li class="day"
                user_id='{{$user->id}}'
                post_date='{{$dt->format('Y-m-d')}}'
                auth_user_id='{{auth()->user()->id}}'
            >
                <div>
                    <span style="cursor: pointer">

                        <i class="material-icons"
                           data-toggle="tooltip"
                           title="This day">
                            <h3 style="padding: 2px">{{$dt->day}}</h3>
                        </i>
                        </span>

                        <sub style="color: red">
                            @if($posts_count[$i]["public_count"]!=0)
                              {{$posts_count[$i]["public_count"]}}
                            @endif
                        </sub>
                    <sub style="color: blue">
                        @if(($posts_count[$i]["private_count"]!=0) && (auth()->user()->id == $user->id))
                            {{$posts_count[$i]["private_count"]}}
                        @endif
                    </sub>

                </div>
            </li>
            @php
                $dt->addDay();
            @endphp
        @endfor
        </ul>
        </form>
    </div>

</div>
@include('modals.post_list')
@include('modals.post_delete')

