<div>
    <h3>Last public activity of {{$user->username}}</h3>
    <div class="row">
            {{\App\Http\Controllers\ProfilesController::public_activity($user)}}
    </div>
</div>
