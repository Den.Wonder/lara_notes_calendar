@extends('layouts.app')

@section('content')
    <div class="container" id="search_section">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Search User's Data</div>
                <div class="panel-body">
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search User's Data" />
                    </div>
                    <div class="table-responsive">
                        <h3 align="center">Total Data : <span id="total_records"></span></h3>
                        <table class="table table-striped table-bordered" id="table_users">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Name</th>
                                <th>email</th>
                                <th>title</th>
                                <th>description</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
