@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="/img/ava_1.jpg" style="max-height: 250px" class="rounded-circle p-5">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>
                    {{ $user->username }}
                </h1>
                @can('update', $user->profile)
                    <a href="/p/create" title="Add new post">Add new post</a>
                @endcan
                <a href="/p/create" title="Add new post">Add new post</a>

            </div>
                @can('update', $user->profile)
                    <a href="/profile/{{$user->id}}/edit" title="Edit profile">Edit profile</a>
                @endcan
            <a href="/profile/{{$user->id}}/edit" title="Edit profile">Edit profile</a>

            <div class="d-flex">
                <div class="pr-5"><strong>
                        {{$user->posts->count()}}
                    </strong> notes</div>
                <div class="pr-5"><strong>21 k.</strong> followers</div>
                <div class="pr-5"><strong>212</strong> following</div>
            </div>
            <div class="pt-4">
                <b>{{$user->profile->title}}</b>

            </div>
            <div>
                {{$user->profile->description}}

            </div>
            <div>
                <a href="/">{{$user->profile->url ?? 'N/A'}}</a>
            </div>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-8">
            here is calendar

        </div>
        <div class="col-4">
            here is posts
        </div>
    </div>
</div>
@endsection
