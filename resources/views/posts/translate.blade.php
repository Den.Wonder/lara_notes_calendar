@extends('layouts.app')

@section('content')
<div class="container">
    <div>
        <div>
            <p><h3>{{$post->title}}</h3></p>
        </div>
        <div>
            <br>
            <p><i>{{$post->caption}}</i></p>
            <br>
        </div>
        <div>
            <p>{{$post->text}}</p>
        </div>

        <div>
            <a href='/p/translate/{{$post->id}}/eng'>
                <button class="btn btn-primary">
                    eng
                </button>
            </a>
            <a href='/p/translate/{{$post->id}}/rus'>
                <button class="btn btn-primary">
                    rus
                </button>
            </a>
        </div>
        <p>
            Переведено сервисом «Яндекс.Переводчик»
            с активной ссылкой на страницу
            http://translate.yandex.ru.
        </p>

    </div>
</div>

@endsection
