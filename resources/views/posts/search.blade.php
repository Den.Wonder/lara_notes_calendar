@extends ('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Post search</div>
                <div class="panel-body">
                    <form method="GET" action="{{url('p/search/action')}}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="search_post" id="search_post" class="form-control" placeholder="Post search" />
                        </div>
                        <button class="btn btn-info">Search</button>
                    </form>
                </div>
                    <div class="table-responsive">
                        <h3 align="center">Total Data : <span id="total_post_records"></span></h3>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Author</th>
                                <th>title</th>
                                <th>caption</th>
                                <th>text</th>
                                <th>last update</th>
                            </tr>
                            @if(count($posts)>0)
                                @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->user_id}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->caption}}</td>
                                    <td>{{$post->text}}</td>
                                    <td>{{$post->updated_at}}</td>
                                </tr>
                                @endforeach
                                @else
                            <tr>
                                <td colspan="5" class="text-center text-danger">
                                    result not found!
                                </td>
                            </tr>
                                @endif
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
