@extends('layouts.app')

@section('content')
<div class="container">
    <div>
        <h2>NEW NOTE</h2>
    </div>
    <form action="/p" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row">
            <div class="col-8 offset-2">
                <div class="form-group row">
                    <label for="title" class="col-md-4 col-form-label text-md-right">Note Title</label>

                    <div class="col-md-6">
                        <input id="title"
                               type="text"
                               class="form-control @error('title') is-invalid @enderror"
                               name="title"
                               value="{{ old('title') }}"
                               required autocomplete="title"
                               autofocus>

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="caption" class="col-md-4 col-form-label text-md-right">Note Caption</label>

                    <div class="col-md-6">
                        <input id="caption" type="text" class="form-control @error('caption') is-invalid @enderror" name="caption" value="{{ old('caption') }}" autocomplete="caption" autofocus>

                        @error('caption')
                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                        @enderror
                    </div>
                </div>
                <div class="md-form form-group row">
                    <label for="Text" class="col-md-4 col-form-label text-md-right">Note Text</label>
                    <div class="col-md-6">
                                <textarea id="text" type="text" class="md-textarea form-control @error('text') is-invalid @enderror" rows="5" name="text" value="{{ old('text') }}" required autocomplete="text" autofocus>

                                </textarea>

                        @error('text')
                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                        @enderror
                    </div>

                </div>
                <div class="form-group row">
                    <div class="form-check ">
                        <div class="col-md-4"></div>
                        <div class="col-md-6 text-md-right">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="materialUnchecked" name="private" autocomplete="private" autofocus value=1>
                                <label class="form-check-label" for="materialUnchecked">Private</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="materialChecked" name="private" checked autocomplete="private" autofocus value=0>
                                <label class="form-check-label" for="materialChecked">Public</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="date">
                        <form>
                            <div class="form-group">
                                <label for="inputDate">enter date:</label>
                                <input type="date" class="form-control" name="date" autocomplete="date" required autofocus>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row pt-4">
                    <button class="btn btn-primary">
                        Add new note
                    </button>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection
