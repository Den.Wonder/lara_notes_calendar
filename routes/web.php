<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/search','SearchController@index')->name('search');
Route::get('/search/action','SearchController@action')->name('search.action');

Route::post('follow/{user}', 'FollowsController@store');

Route::prefix('p/')->group(function (){
    Route::get('/create', 'PostsController@create');
    Route::get('/search', 'PostsController@search')->name('post.search');
    Route::get('/search/action', 'PostsController@search_action')->name('post.search_action');
    Route::get('/search/query', 'PostsController@query')->name('post.query');
    Route::post('/', 'PostsController@store');
    Route::get('/edit/{post}', 'PostsController@edit')->name('post.edit');
    Route::delete('/{post}', 'PostsController@destroy');                    // edit this
    Route::patch('/{post}', 'PostsController@update')->name('post.update');
    Route::get('/translate/{post}', 'PostsController@translate')->name('post.translate');
    Route::get('/translate/{post}/{lang}', 'PostsController@translate_action')->name('post.translate_action');
    // and this
});


Route::prefix('profile/{user}/')->group(function (){
    Route::get('/', 'ProfilesController@index')->name('profile.show');
    Route::post('/', 'ProfilesController@index')->name('profile.show');
    Route::get('/edit', 'ProfilesController@edit')->name('profile.edit');
    Route::patch('/', 'ProfilesController@update')->name('profile.update');
    Route::get('/delete/','ProfilesController@destroy')->name('user.delete');
    Route::get('/{date}', 'ProfilesController@show');
    Route::get('/{target}', 'ProfilesController@toast');
});
