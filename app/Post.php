<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Post extends Model
{

    use SoftDeletes;
    use Searchable;
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function searchableAs(){
        return'posts_index';
    }

//    public function toSearchableArray(){
//        $array = $this->toArray();
//        $data = [
//            'id'=>$array['id'],
//            'title'=>$array['title'],
//            'caption'=>$array['caption'],
//            'text'=>$array['text'],
//            'updated_at'=>$array['update']
//        ];
//
//        return $data;
//    }


}
