<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function profileImage()
    {
        $imagePath = ($this->img) ? $this->img : 'profile/edG40OdnTPluXnN9a7U3pA4vr0WBJMdtt2bYXV62.jpeg';
        return '/storage/'.$imagePath;
    }

    public function followers(){
        return $this->belongsToMany(User::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
