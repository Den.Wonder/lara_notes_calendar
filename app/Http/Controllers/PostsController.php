<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){

        return view('posts.create');
    }

    public function query(){
        $posts = Post::search('title')->get();
        foreach($posts as $post){
        }
    }


    public function search(){
        $posts = Post::get();
        return view('posts.search',[
            'posts'=>$posts
        ]);
    }

    public function search_action(Request $request){
        if($request->has('search')){
            $posts = Post::search($request->get('search'))->get();
        } else {
            $posts = Post::get();
        }
        return view('posts.search',[
            'posts'=>$posts
        ]);
    }

    public function translate($post){
        $post = Post::findOrFail($post);
        return view('posts.translate', [
            'post'=> $post
        ]);
    }

    Public function translate_action($post, $lang){
        if($lang == "rus"){
            $YT_lang = 'en-ru';
        } elseif($lang == "eng"){
            $YT_lang = 'ru-en';
        }

        $post = Post::findOrFail($post);
        $YT_api_key='trnsl.1.1.20191004T084033Z.01f5fe1df3fe0200.74a0886703e6f3bc71ab402cad1e3c3dcfabb99b';
        $YT_title=urlencode($post->title);
        $YT_caption=urlencode($post->caption);
        $YT_text = urlencode($post->text);
        $YT_link_title = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=".$YT_api_key."&text=".$YT_title."&lang=".$YT_lang;
        $YT_link_caption = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=".$YT_api_key."&text=".$YT_caption."&lang=".$YT_lang;
        $YT_link_text = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=".$YT_api_key."&text=".$YT_text."&lang=".$YT_lang;
        $result = $post;
        $result->title=file_get_contents($YT_link_title);
        $result->title=json_decode($result->title,true);
        $result->title=$result->title['text'][0];
        $result->caption=file_get_contents($YT_link_caption);
        $result->caption=json_decode($result->caption,true);
        $result->caption=$result->caption['text'][0];
        $result->text=file_get_contents($YT_link_text);
        $result->text=json_decode($result->text,true);
        $result->text=$result->text['text'][0];

       return view('posts.translate',[
           'post'=>$result
       ]);


    }


    public function edit(Post $post){
        $user = Auth()->user();
        if($user->can('update', $post)) {

            return view('posts.edit', [
                'post' => $post
            ]);
        } else {
            ProfilesController::toast('post_warn');
        };
    }

    public function update($post){
        $post = Post::find($post);
        $data = request()->validate([
           'title'=>'required',
           'caption'=>'',
           'text'=>'required',
           'date'=>'required',
           'private'=>'required',
        ]);
        $post->update($data);
        toastr()->success('Post was successfull edited!');
        return redirect("/profile/".auth()->user()->id);

    }

    public function destroy(Post $post){
        $user = Auth()->user();
        if ($user->can('delete',$post)) {
            $post = Post::findOrFail($post->id);
            $post->delete();
            toastr()->success('Post was deleted');
        } else {
            toastr()->error('something wrong');

        }
        return redirect('/profile/' .auth()->user()->id);
       // return view('/profile/'.auth()->user()->id);
    }

    public function store(){
        $data = request()->validate([
            'title'=>'required',
            'caption'=>'',
            'text'=>'required',
            'private'=>'required',
            'date'=>'required'
        ]);

        auth()->user()->posts()->create($data);


        return redirect('/profile/' .auth()->user()->id);

    }

}
