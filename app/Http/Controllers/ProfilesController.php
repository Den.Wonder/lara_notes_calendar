<?php

namespace App\Http\Controllers;

use App\Post;
use App\Profile;
use App\User;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use TJGazel\Toastr\Facades\Toastr;


class ProfilesController extends Controller
{


    public function index($user, Request $request){
        $user =User::with('posts', 'profile')->findOrFail($user);
        $follows=(auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        if (!($request->date)){
            $current_date = date('Y-m-j', time());
        } else {
            $current_date = $request['date'];
        }
        $input_date = Carbon::createFromFormat('!Y-m-j', $current_date, 'Asia/Krasnoyarsk');
        $weeks_before = Carbon::createFromFormat('!Y-m-j', $current_date)->subWeeks(2);
        $weeks_after= Carbon::createFromFormat('!Y-m-j', $current_date)->addWeeks(2);
        $period = Carbon::parse($weeks_before)->daysUntil($weeks_after);
        $start_date = $weeks_before;
        $start_date->format('Y-m-j');
        $posts_count = [0=>$this->posts_in_day($start_date, $user->id)];
        foreach ($period as $key=>$date){
            if($key){
                $day = $date->format('Y-m-j');
                $day_mark = $this->posts_in_day($day, $user->id);
                array_push($posts_count,$day_mark);

            }
        }
        $str_date = $input_date->format('Y-m-j');
        $dt = $weeks_before;
        $days_count = $period->count();
        toastr()->info('welcome to '.$user->username.' page!', null, ['timeout'=>3000] );
        $headings = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
        return view('profiles.index', [
            'user'=>$user,
            'follows'=>$follows,
            'input_date'=>$input_date,
            'str_date'=>$str_date,
            'period'=>$period,
            'weeks_before'=>$weeks_before,
            'weeks_after'=>$weeks_after,
            'headings'=>$headings,
            'dt'=>$dt,
            'days_count'=>$days_count,
            'posts_count'=>$posts_count
        ]);
    }


    public function show(Request $request){
        $user_id = (int)$request->user_id;
        $post_date = $request->post_date;
        toastr()->info('look at the posts!');
        if (auth()->user()->id == $request->user_id) {
            $posts = Post::where('user_id', '=', $user_id)->where('date', '=', $post_date)->get();
        } else {
            $posts = Post::where('user_id', '=', $user_id)->where('date', '=', $post_date)->where('private','=',0)->get();
        }
        return response()->json([
            'input_date'=>$post_date,
            'posts'=>$posts
        ],200);
    }

    private function posts_in_day($day, $user_id){
        $pub_posts = Post::where('date','=',$day)->where('private','=',0)->where('user_id','=',$user_id)->get();
        $public_post_count = count($pub_posts);
        if(auth()->user()->id == $user_id){
            $priv_posts = Post::where('date','=',$day)->where('private','=',1)->where('user_id','=',$user_id)->get();
            $private_post_count = count($priv_posts);
        } else {
            $private_post_count = 0;
        }

        $day_mark =[
            'day'=> $day,
            'private_count'=>$private_post_count,
            'public_count'=>$public_post_count
        ];
           return $day_mark;

    }

    public function edit($user){
        $user = User::with('profile')->find($user);
        toastr()->success($user->username.', your profile was successfully edited!','PROFILE UPDATE',['timeout'=>3000]);

        $this->authorize('update', $user->profile);
        return view('profiles.edit',[
            'user'=>$user
        ]);
    }

    public function update($user){
        $user = User::with('profile')->find($user);
        $this->authorize('update', $user->profile);
        toastr()->success($user->username.', your profile was successfully edited!','PROFILE UPDATE',['timeout'=>3000]);
        $data = request()->validate([
            'title'=>'required',
            'description'=>'required',
            'url'=>'url',
            'img'=>'',
        ]);

        if (request('img')) {
            $imagePath = request('img')->store('profile', 'public');
            $img = Image::make(public_path("storage/{$imagePath}"))->fit(500, 500);
            $img->save();
            $imageArray = ['img' => $imagePath];
        }
        auth()->user()->profile->update(array_merge(
            $data,
            $imageArray ?? []
        ));

        return redirect("/profile/{$user->id}");
    }

    public static function public_activity(User $user){
        $last_posts = Post::where('user_id','=',$user->id)->where('private','=',0)->orderByDesc('updated_at')->get();
        if(count($last_posts)<5){
            $count_of_activity = count($last_posts);
        } else {
            $count_of_activity = 5;
        }
        for($i=0;$i<$count_of_activity;$i++){
            if($last_posts[$i]['created_at'] != $last_posts[$i]['updated_at']){
                $activity = 'updated';
                $activity_time = $last_posts[$i]['updated_at'];
            } else {
                $activity = 'created';
                $activity_time = $last_posts[$i]['created_at'];

            }
            echo "<div class='col-4 post-card p-3'><p> Post <b>".
                $last_posts[$i]['title'] ."</b> was ". $activity .
                "<br> at " . $activity_time .
                "<p><b>". $last_posts[$i]['title'] ."</b></p>" .
                "<p><i>". $last_posts[$i]['caption'] ."</i></p>" .
                "<p>". $last_posts[$i]['text'] ."</p>" .
                "</div>";
        }
    }

    public function destroy(User $user){

        $user = User::findOrFail($user->id);
        $user->posts()->delete();
        $user->profile()->delete();
        $user->delete();
        return redirect('/login/');

    }




}
