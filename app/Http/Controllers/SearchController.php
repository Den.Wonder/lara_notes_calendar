<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function index(){
        return view('search');
    }
    public function action(Request $request){
        if($request->ajax()){
            $output = '';
            $query = $request->get('query');
            if($query!=''){
                //worked, but with only one table
//                $data = DB::table('users')
//                    ->where('username','like','%'.$query.'%')
//                    ->orWhere('name','like','%'.$query.'%')
//                    ->orWhere('email','like','%'.$query.'%')
//                    ->orderBy('id','asc')
//                    ->get();

                //good worked
                $data = User::with('profile')->where('username','LIKE','%'.$query.'%')
                    ->orWhere('name','LIKE','%'.$query.'%')
                    ->orWhere('email','LIKE','%'.$query.'%')
                    ->orWhereHas('profile',function ($q) use ($query){
                        return $q->where('title','LIKE','%'.$query.'%')
                            ->orWhere('description','LIKE','%'.$query.'%');
                    })
                    ->orderBy('id','asc')
                    ->get();


//                  $data = User::with(['profile'=>function($query){
//                      $query->where('title','LIKE','%'.$query.'%')
//                          ->orWhere('description','LIKE','%'.$query.'%');
//
//                  }])
//                      ->orWhere('username','LIKE','%'.$query.'%')
//                      ->orWhere('name','LIKE','%'.$query.'%')
//                      ->orWhere('email','LIKE','%'.$query.'%')
//                      ->orderBy('id','asc')
//                      ->get();


            } else {
                $data = User::with('profile')
                    ->orderBy('id','asc')
                    ->get();
            }
            $total_row = $data->count();
            if($total_row>0){
                foreach ($data as $row){
                    $output .='
                    <tr>
                        <td>'.$row->username.'</td>
                        <td>'.$row->name.'</td>
                        <td>'.$row->email.'</td>
                        <td>'.$row->profile->title.'</td>
                        <td>'.$row->profile->description.'</td>
                    </tr>
                    ';
                }
            } else {
                $output = '
                <tr>
                    <td align="center" colspan="5">No data found</td>
                </tr>
                ';
            }
            $data = array(
                'table_data'=>$output,
                'total_data'=>$total_row
            );


            echo json_encode($data);


        }
    }
}
