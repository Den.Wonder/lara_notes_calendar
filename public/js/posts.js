
    function showPostForm(user_id, post_date, auth_user_id ){
        $.ajax({
            type: 'GET',
            url: '/profile/'+user_id+'/'+post_date,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'user_id': user_id,
                'post_date': post_date,
                'auth_user_id': auth_user_id
            },

            success: function(data) {
                if(data.posts.length == 0){
                    var list_content = "<div> No posts  " + data.input_date + "</div>";
                } else {
                    var list_content = "<div>";
                    for(var i=0;i<data.posts.length;i++){
                        var obj = data.posts[i];
                        console.log('here im');
                        var post_id = obj.id;
                        var card = "<div class='card p-3'><h3>" + obj.title + "</h3>" +
                            "<h4>" + obj.caption + "</h4>"
                            + "<p>" + obj.text + "</p>"
                            + "<p><a href='/p/translate/" +obj.id+"/' title='translate-page'>look at the page!</a></p>"
                        ;
                        var post_title = obj.title;
                        if (obj.user_id == auth_user_id) {
                            card +=
                                 "<div class='d-flex'><a href='/p/edit/" + obj.id + "' title='edit post'<button class='btn btn-primary mr-5'>edit</button></a> "
                                + "<a onclick='delete_post(" + post_id + ",`" + post_title + "`,`" + auth_user_id + "`)' class='delete'><button class='btn btn-danger'>delete</button></a>"
                                + "</p></div></div>";
                        } else {
                            card += "</div>";
                        }
                            list_content += card;

                    }
                    list_content += '</div>';
                }
                console.log(list_content);
                $('#title').html(list_content);
                console.log(data);


                var str_for_date = data.input_date[8] + data.input_date[9]+ '.' + data.input_date[5] + data.input_date[6];
               console.log('str_for_date: ' + str_for_date);
                $('#date_of').html(str_for_date);
                $('#showPostModal').modal('show');
            }
        })
    }


    function delete_post(post_id, title, auth_user_id) {
        $.ajax({
            url: '/p/'+post_id,
            method:'delete',
            type: 'get',
            data:{
                'post_id' : post_id,
                'title': title,
                'method':'delete',
                 "_token":$('meta[name="csrf-token"]').attr('content')

            },
            success: function () {
                console.log('deleted function is calling successfully!');
                console.log('for post id' + post_id);
                $('#showPostModal').modal('hide');
                $('#deleted_data').html(title);
                $('#deletedPostModal').modal('show');


            },
            error: function () {
                console.log('error. deleted func is calling, but falling')
                console.log('for post id' + post_id);
                $('#showPostModal').modal('hide');
                console.log('how it possible?');
                $('#deleted_data').html(title);
                $('#deletedPostModal').modal('show');
                console.log('how it possible?222222222');

            }
        })
    }
